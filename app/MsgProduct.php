<?php

namespace App;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MsgProduct extends Model
{
    use SoftDeletes;
    protected $fillable=['user_id','product_id','msg_content',];
}
