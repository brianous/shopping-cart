<?php
namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use Auth;

class  UserController extends Controller{

//   Sign UP
    public function postSignUp(Request $request)
    {
        $this->validate($request,[
            'email' =>'required|email|unique:users',
            'password'=>'required|min:4',
            'birthday'=>'date',
        ]);

        $user = new User([
            'email' =>  $request['email'],
            'password' => Hash::make($request['password']),
            'name'=> $request['name']? $request['name'] :'none',
            'birthday'=>$request['birthday'],
        ]);
        $user->save();

        Auth::login($user);
        return redirect('/');
    }

// Sign In
    public function postSignIn(Request $request)
    {
        if( Auth::attempt([
        'email' => $request['email'],
        'password' => $request['password']  ])  
        ){
            return redirect() -> route('user.profile');
        }
        return redirect()->back();

    }
// Profile
    public function getProfile()
    {   
        $user = Auth::user();
        $email = $user->email;
        $emailCut = substr_replace($email, str_repeat('*', strcspn($email,'@')-2), 2, strcspn($email,'@')-2);
        $name = $user->name;
        $birthday = $user->birthday;
        
        $inf = array('email' => $emailCut, 'name' => $name, 'birthday' => $birthday);
        return view('user.profile', $inf);

    }

    public function postEditProfile(Request $request)
    {   
        $this->validate($request,[
            'birthday'=>'date',
        ]);

        $user = Auth::user();
        $user->name =$request['name'];
        $user->birthday =$request['birthday'];
        $user->save();

        return redirect() -> route('user.profile');
    }



// log out
    public function getLogout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect()->back();
    }
}