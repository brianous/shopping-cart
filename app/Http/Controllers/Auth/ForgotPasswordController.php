<?php

namespace App\Http\Controllers\Auth;

// use Illuminate\Foundation\Auth\RegistersUsers;
// use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Password;

// use Illuminate\Notifications\Notifiable;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    // use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    
    // public function broker()
    // {
    //     return Password::broker();
    // }


    public function postPasswordForgot(Request $request)
    {

        // 1. check email

        // $validator = Validator::make($request->all(),[
        //     'email' =>'required|email',
        // ]);

        // if($validator->fails()){
        //     dd("fails");
        // }else{
        //     dd("passes");
        // }
 
        // $user = User::whereEmail($request->email)->first();
        
        // if($user == null){
        //     dd("fails");
        // }




        $request->validate(['email' => 'required|email']);

        $status = Password::sendResetLink( $request->only('email') );
    
        return $status === Password::RESET_LINK_SENT
                    ? redirect()->route('home')->with(['status' => __($status)])
                    : back()->withErrors(['email' => __($status)]);

    }
}
