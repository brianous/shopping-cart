<?php

namespace App\Http\Controllers;

use App\Product;
use App\Cart;
use App\MsgProduct;

use Illuminate\Http\Request;
use App\Http\Requests; 
use Session;

class ProductController extends Controller
{
    public function getIndex(){
        $products = Product::all();
        return view('shop.index',['products' => $products]);
    }
    public function getCart()
    {
        if(!Session::has('cart')){
            return view('shop.shopping-cart',['products' => null]);    
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        return view('shop.shopping-cart',['products'=>$cart->items,'totalPrice'=>$cart->totalPrice]);    
    }

    public function getProductPage($id)
    {
        $product = Product::find($id);
        $msgs = MsgProduct::where('product_id',$id)->get();
        return view('shop.product-page',['product' => $product,'msgs'=>$msgs]);
    }

    public function postMsg(Request $r,$id)
    {
        $uid = $r->user()->id;
        $msg = $r['msg'];

        $msg_product = new MsgProduct([
            'user_id' => $uid,
            'product_id'=> $id,
            'msg_content' => $msg,
        ]);
        $msg_product->save();
        return redirect()->back();
    }
    public function getAddToCart($id)
    {
        $product = Product::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null ;
        $cart = new Cart($oldCart);
        $cart->add($product, $id);
        Session::put('cart',$cart);
        return redirect()->back();
    }

    public function getReduceByOne($id)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->reduceByOne($id);

        if(count($cart->items) >0){
            Session::put('cart',$cart);
        }else{
            Session::forget('cart');
        }
        
        return redirect()->route('product.shoppingCart');
    }

    public function getRemoveItem($id)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);

        if(count($cart->items) >0){
            Session::put('cart',$cart);
        }else{
            Session::forget('cart');
        }

        return redirect()->route('product.shoppingCart');
    }



    public function getCheckout()
    {
        if(!Session::has('cart')){
            return view('shop.shopping-cart');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $totalPrice = $cart->totalPrice;
      
        return view('shop.checkout',['total' => $totalPrice]);
    }
}
