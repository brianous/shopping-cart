<?php

namespace App;

class Cart
{
    public $items = null;
    public $totalQuantity = 0;
    public $totalPrice = 0;

    public function __construct($oldCart)
    {
        if($oldCart)
        {
            $this->items = $oldCart->items;
            $this->totalQuantity = $oldCart->totalQuantity;
            $this->totalPrice = $oldCart->totalPrice;
        }
    }

    // add item to Cart
    // first. 
    //      Initialize a var = ItemInCart
    // second.
    //       Q：If the Item is already in the Cart?
    //          Yes, restore the data
    // third. 
    //      User add the Item to the cart, so quantity ++
    //      Ite

    // price = quantity * price
    public function add($item, $id)
    {
        $itemInCart = ['quantity'=> 0, 'price'=> $item->price, 'item'=> $item];

        if($this->items)
        {
            if(array_key_exists($id, $this->items)){
                $itemInCart = $this->items[$id];
            }
        }
        
        $itemInCart['quantity']++;
        $itemInCart['price'] = $item->price * $itemInCart['quantity'];
        $this->items[$id] = $itemInCart;
        $this->totalQuantity++;
        $this->totalPrice += $item->price;
    }

    // price =  quantity * price
    public function reduceByOne($id)
    {
        $this->items[$id]['quantity']--;
        $this->items[$id]['price'] -= $this->items[$id]['item']['price'];
        $this->totalQuantity--;
        $this->totalPrice -= $this->items[$id]['item']['price'];

        if($this->items[$id]['quantity']<=0){
            unset($this->items[$id]);
        }
    }


    public function removeItem($id)
    {
        $this->totalQuantity -= $this->items[$id]['quantity'];
        $this->totalPrice -=  $this->items[$id]['price'];
        unset($this->items[$id]);       
    }

}
    // public function totalPrice()
    // {
    //     # code...
    // }



