<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web']], function () {
    


    Route::get('/', [
        'uses'=>'ProductController@getIndex',
        'as'  =>'home'
    ]);


    Route::get('/add-to-cart/{id}', [
        'uses'=>'ProductController@getAddToCart',
        'as'  =>'product.addToCart'
    ]);

    Route::get('/product-page/{id}', [
        'uses'=>'ProductController@getProductPage',
        'as'  =>'product.get-product-page'
    ]);

    Route::get('/product-reduce-by-one/{id}', [
        'uses'=>'ProductController@getReduceByOne',
        'as'  =>'product.productReduceByOne'
    ]);

    Route::get('/product-remove-item/{id}', [
        'uses'=>'ProductController@getRemoveItem',
        'as'  =>'product.productRemoveItem'
    ]);

    Route::get('/shopping-cart', [
        'uses'=>'ProductController@getCart',
        'as'  =>'product.shoppingCart'
    ]);

    Route::get('/checkout', [
        'uses'=>'ProductController@getCheckout',
        'as'  =>'checkout'
    ]);

    Route::post('/checkout', [
        'uses'=>'ProductController@postCheckout',
        'as'  =>'checkout'
    ]);
        
// user gorup
    Route::group(['prefix' => 'user'], function () {
        
        //guest
        Route::group(['middleware' => 'guest'], function () {
            Route::get('/signup',[
                'as'  => 'user.signup',
                function(){return view('user.signup');}
            ]);
            Route::post('/signup',[
                'uses'=> 'UserController@postSignUp',
                'as'  => 'user.signup'
            ]);
        
            Route::get('/signin',[
                'as'  => 'user.signin',
                function(){return view('user.signin');}
            ]);
            Route::post('/signin',[
                'uses'=> 'UserController@postSignIn',
                'as'  => 'user.signin'
            ]);
            
            Route::get('/forgot-password',[
                'as'  => 'user.forgot-password',
                function(){return view('user.forgot-password');}
            ]);
            Route::post('/forgot-password',[
                'uses'=> 'Auth\ForgotPasswordController@postPasswordForgot',
                'as'  => 'user.forgot-password'
            ]);

            Route::get('/reset-password/{token}', function ($token) {
                return view('user.reset-password', ['token' => $token]);
            })->name('password.reset');

            // Route::get('/reset-password/{token}',[
            //     'as'  => 'password.reset',
            //     function($token){
            //         return view('user.reset-password', ['token' => $token]);
            //     }
            // ]);

            Route::post('/reset-password',[
                'uses'=> 'Auth\ResetPasswordController@postResetPassword',
                'as'  => 'password.update'
            ]);
            
            
        });
        // user signed in
        Route::group(['middleware' => 'auth'], function () {
            
            Route::get('/profile',[
                'uses'=> 'UserController@getProfile',
                'as'=> 'user.profile',
                ]);
            Route::post('/edit/profile',[
                'uses'=> 'UserController@postEditProfile',
                'as'=> 'user.edit.profile',
                ]);
            Route::post('/product/msg/{id}',[
                'uses'=> 'ProductController@postMsg',
                'as'=> 'msg.product',
                ]);

            Route::get('/logout',[
                'uses'=> 'UserController@getLogout',
                'as'  => 'user.logout',
            ]);
        });

    });

        
});
