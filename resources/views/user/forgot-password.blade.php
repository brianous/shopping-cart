@extends('layouts.master')

@section('content') 

   {{-- User.Forget-password --}}
  <div class="card mt-5" style="margin:0 auto;">
    <div class="card-body">
        <div class="d-flex justify-content-center">
        <div class="col-md-4 col-md-offset-4">
            <h1>Password assistance</h1>
            <div>
                <p>Enter the email address associated with your account.</p>
            </div>
            <form action="{{ route('user.forgot-password') }}" method="POST">
                <div class="form-group">
                    <input placeholder="Your E-mail" class="form-control" type="text" name="email" id="email" required>
                </div>
                    
                <button class="btn btn-primary btn-block" type="submit" >Continue</button>
                {{ csrf_field() }}
                <input type="hidden" name="_token" value="{{ Session::token() }}">
            </form>

        </div>
        </div>
    </div>
</div>

@endsection