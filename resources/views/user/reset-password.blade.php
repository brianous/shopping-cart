@extends('layouts.master')

@section('content') 

   {{-- User.Reset-password --}}
  <div class="card mt-5" style="margin:0 auto;">
    <div class="card-body">
        <div class="d-flex justify-content-center">
        <div class="col-md-4 col-md-offset-4">
            
            @if(count($errors) > 0 )
            <div class="row">
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            </div>
            @endif

            <h1>Reset-password</h1>
            <div>
                <p>Please Enter a new password</p>
            </div>
            <form action="{{ route('password.update') }}" method="POST">
                
                <div class="form-group">
                    <input placeholder="Your E-mail" class="form-control" type="text" name="email" id="email" required>
                </div>
                <div class="form-group">
                    <input placeholder="Your New Password" class="form-control" type="password" name="password" id="password" required>
                </div>
                <div class="form-group">
                    <input placeholder="Your New Password" class="form-control" type="password" name="password_confirmation" id="password_confirmation " required>
                </div>

                <button class="btn btn-primary btn-block" type="submit" >Reset my password</button>
                {{ csrf_field() }}
                <input type="hidden" name="_token" value="{{ Session::token() }}">
                <input type="hidden" name="token" value="{{$token}}">
            </form>

        </div>
        </div>
    </div>
</div>

@endsection