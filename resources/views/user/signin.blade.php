@extends('layouts.master')

@section('content') 
  
  {{-- Sing in --}}
  <div class="card mt-5" style="margin:0 auto;">
    <div class="card-body">
        <div class="d-flex justify-content-center">
        <div class="col-md-4 col-md-offset-4">
            <h1>Sign In</h1>
            @if(count($errors) > 0 )
            <div class="row">
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            </div>
            @endif

            <form action="{{ route('user.signin') }}" method="post">
                <div class="form-group {{ $errors->has('email') ? 'has-error': ''}}">
                    <input placeholder="Your E-mail" class="form-control" type="text" name="email" id="email" value="{{ Request::old('email')}}" required>
                </div>
                    
                <div class="input-group mb-3  {{ $errors->has('password') ? 'has-error': ''}}">
                    <input placeholder="Your Password" class="form-control" type="password" name="password" id="password" value="{{ Request::old('password')}}" required>
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button" id="password_toggle" onclick="passwordToggle()">
                           顯示 
                        </button>
                    </div>
                </div>

                <button class="btn btn-primary btn-block" type="submit" >Sign In</button>
                {{ csrf_field() }}
                <input type="hidden" name="_token" value="{{ Session::token() }}">
            </form>
            <div style="text-align:center; padding-top:10px;">
                <a href=" {{ route('user.forgot-password') }} "> Forgot password?</a><br><br>
                <a href=" {{ route('user.signup') }} "> have no account?  Sing Up</a>
            </div>


        </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script>
        function passwordToggle() {
            var password = document.getElementById("password");
            var password_toggle = document.getElementById("password_toggle");
            if (password.type === "password") {
                password_toggle.textContent="隱藏";
                password.type = "text";
            } else {
                password_toggle.textContent="顯示";
                password.type = "password";
            }
        }
    </script>
@endsection
