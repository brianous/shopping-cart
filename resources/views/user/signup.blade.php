@extends('layouts.master')

@section('content') 
  
  {{-- Sing up --}}
 
<div class="card mt-5" style="margin:0 auto;">
    <div class="card-body">
        <div class="d-flex justify-content-center">
        <div class="col-md-4 col-md-offset-4">
            <h1>Sign Up</h1>
            @if(count($errors) > 0 )
                <div class="row">
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                </div>
            @endif

            <form action="{{ route('user.signup') }}" method="post">
                <div class="form-group {{ $errors->has('email') ? 'has-error': ''}}" >
                    <input placeholder="Your E-mail" class="form-control" type="text" name="email" id="email" value="{{ Request::old('email')}}" required>
                </div>
            

                <div class="input-group mb-3  {{ $errors->has('password') ? 'has-error': ''}}">
                    <input placeholder="Your Password" class="form-control" type="password" name="password" id="password" value="{{ Request::old('password')}}" required>
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button" id="password_toggle" onclick="passwordToggle()">
                           顯示 
                        </button>
                    </div>
                  </div>
                  
                  
                <div class="form-group {{ $errors->has('name') ? 'has-error': ''}}">
                    <input placeholder="Your Name" class="form-control" type="text" name="name" id="name" value="{{ Request::old('name')}}" >
                </div>

                <div class="form-group {{ $errors->has('birthday') ? 'has-error': ''}}">
                    <input placeholder="Your Birthday" class="form-control" type="text" name="birthday" id="birthday" onfocus="(this.type='date')" value="{{ Request::old('birthday')}}">     
                </div>

                <button class="btn btn-primary btn-block" type="submit" >Sign Up</button>
                {{ csrf_field() }}
                <input type="hidden" name="_token" value="{{ Session::token() }}">
            </form>

            <div style="text-align:center;">
                <a href=" {{ route('user.signin') }} "> already have a account?  Sing In</a>
            </div>

        </div>
        </div>
    </div>
</div>


@endsection

@section('js')
    <script>
        function passwordToggle() {
            var password = document.getElementById("password");
            var password_toggle = document.getElementById("password_toggle");
            if (password.type === "password") {
                password_toggle.textContent="隱藏";
                password.type = "text";
            } else {
                password_toggle.textContent="顯示";
                password.type = "password";
            }
        }
    </script>
@endsection