@extends('layouts.master')

@section('content')


<div class="card mt-5" style="margin:0 auto;">
    <div class="card-body">
        <div class="d-flex justify-content-center">
        <div class="col-md-4 col-md-offset-4">
            <h1>Profile</h1>
            @if(count($errors) > 0 )
            <div class="row">
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            </div>
            @endif

            <form action=" {{ route('user.edit.profile') }} " method="post">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">E-mail</span>
                    </div>
                    <input placeholder="E-mail" class="form-control" type="text" name="email" id="email" value="{{ $email }}" disabled>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Name</span>
                    </div>
                    <input placeholder="Name" class="form-control" type="text" name="name" id="name" value="{{ $name }}" disabled>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">birthday</span>
                    </div>
                    <input placeholder="birthday" class="form-control" type="date" name="birthday" id="birthday" value="{{ $birthday }}" disabled>
                </div>

                <button class="btn btn-primary btn-block" type="submit">Edid profile</button>
                {{ csrf_field() }}
                <input type="hidden" name="_token" value="{{ Session::token() }}">
            </form>

        </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script>
        var input_name = document.getElementById('name');
        var input_birthday = document.getElementById('birthday');
        $("button[type='submit']").click(function(e) {
            if(input_name.disabled){
                e.preventDefault();
                input_name.disabled=false;
                input_birthday.disabled=false;
            }
            console.log("ininiini");
        });
    </script>
@endsection