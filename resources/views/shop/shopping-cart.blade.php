@extends('layouts.master')

@section('title')
    Shopping-Cart (ﾉ>ω<)ﾉ 
@endsection 

@section('content') 

    @if (Session::has('cart'))
        <div class="row justify-content-center">

            <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">

                <table class="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Product</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Price</th>
                      </tr>
                    </thead>
                        @foreach ($products as $product)
                        <tr>
                            <th scope="row"></th>
                            <td>
                                <strong style="font-size:1.5rem;">{{ $product['item']['title'] }}</strong>
                                <img src="{{ $product['item']['imgPath']}}" style="max-height:100px;" alt="productImage" class="rounded product-img">
                                
                            </td>
                            <td>
                                <div class="row-sm-3"style="font-size:1.5rem;">
                                    {{-- + plus --}}
                                    <a href="{{ route('product.productReduceByOne',['id'=>$product['item']['id'] ]) }}">
                                        <i class="fa fa-minus" aria-hidden="true" style="color: rgb(129, 194, 129);"></i>
                                    </a>
                                    <button type="button" class="btn btn-outline-dark" style="margin:0 5px 0 5px;"disabled> 
                                        {{ $product['quantity'] }}
                                    </button>
                                    {{-- - minus --}}
                                    <a href="{{ route('product.addToCart',['id'=>$product['item']['id'] ]) }}">
                                        <i class="fa fa-plus" aria-hidden="true" style="color: rgb(129, 194, 129);"></i>
                                    </a>
                                </div>
                            </td>
                            <td>
                                <div style="font-size:1.5rem;">
                                    <span style="margin:0 20px 0 0; padding:0 10px 0 10px; border-radius:5px;">  {{ $product['price'] }} </span>
                                    {{-- X delete --}}
                                    <a href="{{ route('product.productRemoveItem',['id'=> $product['item']['id'] ]) }}">
                                        <i class="fa fa-times"aria-hidden="true" style="color: rgb(129, 194, 129);"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    <tbody>
                    </tbody>
                </table>

            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
                <strong> Total Price: $ {{$totalPrice}} </strong>
            </div>
        </div>
        <hr>
        <div class="row justify-content-center">
            <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
                <a href="{{ route('checkout') }}" type="button" class="btn btn-success">Checkout</a>
            </div>
        </div>

    @else
    <div class="card mt-5" style="margin:0 auto;">
        <div class="card-body">
            <div class="d-flex justify-content-center">
                <h3>No Items in cart</h3>
            </div>
        </div>
    </div>
    @endif
@endsection
