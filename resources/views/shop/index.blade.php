@extends('layouts.master')

@section('title')
    Shopping-Cart (ﾉ>ω<)ﾉ 
@endsection 

@section('style')
    <style  type="text/css">
    .ellipsis {
        overflow:hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }

    </style>
@endsection

@section('content') 
@foreach ($products->chunk(4) as $productChunk)
        <div class="row">

            @foreach ($productChunk as $product)
                <div class="col-sm-4 col-md-3">
                    <div class="border">

                        <div class="d-flex justify-content-center">
                            <a href="{{ route('product.get-product-page',['id'=>$product->id])}}">
                                <img src="{{ $product->imgPath }}" alt="productImage" class="card-img-top border-bottom">
                            </a>
                        </div>
                        
                        <div class="p-4">
                            <div class="caption" >
                                <a href="{{ route('product.get-product-page',['id'=>$product->id])}}"> 
                                    <h3 class="ellipsis">{{ $product-> title }}</h3> 
                                </a>
                                <p class="product-description ellipsis">{{ $product->description }}</p>
                            </div>
                            <div class="clearfix">
                                <div class="pull-left price">${{ $product->price }}</div>
                                <a href="{{ route('product.addToCart',['id'=>$product->id]) }}" class="btn btn-success pull-right" role="button">Add to Cart</a>
                            </div>
                        </div>
                        
                    </div>
                </div>
            @endforeach

            {{-- @foreach ($productChunk as $product)    
                <div class="card" style="width: 18rem;">
                    <img src="{{ $product->imgPath }}" class="card-img-top" alt="productImage">
                    <div class="card-body">
                        <h5 class="card-title">{{ $product-> title }}</h5>
                        <p class="card-text">{{ $product->description }}</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
            @endforeach --}}

        </div>
@endforeach
@endsection
