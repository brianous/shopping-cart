@php
    use App\User;
@endphp


@extends('layouts.master')

@section('title')
    Shopping-Cart (ﾉ>ω<)ﾉ 
@endsection 

@section('style')
    <style  type="text/css">
    .multi-line {
        word-wrap:break-word;
        white-space:line;
    }
    /* .msg-title{
        font-weight:bold;
        padding: 10px 0 0 20px;
    } */
    .msg{
        line-height: 100%;
        color:gray;
        padding: 10px 0 0 20px;
    }

    .ellipsis {
        overflow:hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }

    </style>
@endsection

@section('content') 
<div class="container"  style="max-width:1000px;">
    
    <div class="border shadow p-3 mb-5 bg-white rounded multi-line">
        <div class="d-flex flex-column bd-highlight mb-3">
            <div style="text-align: center;">
                <img src="{{ $product->imgPath }}" alt="productImage" style="max-width: 500px;">
            </div>
            <h3 style="padding:0 40px 0 40px;">{{ $product-> title }}</h3>
            <p style="padding:0 40px 0 40px;">{{ $product->description }}</p>
            
                            
            <div class=" price" style="text-align: center;">${{ $product->price }}</div>
            <a href="{{ route('product.addToCart',['id'=>$product->id]) }}" class="btn btn-success" role="button">Add to Cart</a>
        </div>
    </div>

    {{-- <button type="button" class="btn btn-primary" id="showToast">Show live toast</button>
    <div class="position-fixed top-0 right-0 p-3" style="z-index: 5; right: 0; top: 50px;">
        <div id="myToast" class="toast hide" role="alert" aria-live="assertive" aria-atomic="true" data-delay="5000">
            <div class="toast-header">
                <strong class="mr-auto"></strong>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body multi-line">
                Add product to Cart, Successfully!        
            </div>
        </div>
    </div> --}}

  
    <div class="border shadow p-3 mb-2 bg-white rounded multi-line">
        <h3 class="border-bottom border-gray pb-2 mb-0"style="text-align: center;">Message</h3>

        @foreach ($msgs->chunk(1) as $msgChunk)
        @foreach ($msgChunk as $msg)

            <div class="msg border-bottom border-gray">
            <p>
                <strong class="d-block text-gray-dark multi-line">{{User::select('email')->where('id',$msg->user_id)->firstOrFail()->email}}</strong>
                {{ $msg->msg_content }}
                <br>
                <small class="pull-right">
                    {{$msg->updated_at}}
                </small>
            </p>
            </div>
        @endforeach
        @endforeach
    </div>

    @if (Auth::check())
        <div class="border shadow p-3 bg-white rounded multi-line">
            <form action="{{ route('msg.product',['id'=>$product->id]) }}" method="POST">
                @csrf
                <div class="form-group">
                    <textarea class="form-control" id="msg" name="msg" type="text" rows="3" placeholder="Write your message here!" required></textarea>
                </div>
                <button class="btn btn-secondary btn-block" type="submit" >Send</button>
            </form>
        </div>
    @endif

@endsection


@section('js')

    <script>
    $(document).ready(function(){
        $("#showToast").click(function(){
            $("#myToast").toast('show');
        });
    });
    </script>

@endsection

