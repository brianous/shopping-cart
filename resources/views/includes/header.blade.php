<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href=" {{ route('home') }} ">Brand</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse"
          data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <form  class="form-inline my-2 my-lg-0 mr-auto" >
                {{-- <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button> --}}
            </form>
            <ul class="navbar-nav">
                <li class="nav-item">
                <a class="nav-link" href=" {{route('product.shoppingCart') }} ">
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i> ShopCart
                    <span class="badeg">{{ Session::has('cart') ? Session::get('cart')->totalQuantity : '' }}</span>
                    </a>
                </li>

                @if (Auth::check())

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user-o" aria-hidden="true"></i>
                        使用者名稱(預定)
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('user.profile') }}">profile</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href=" {{ route('user.logout') }} ">Logout</a>
                    </div>
                </li>

                @else

                     {{-- <a class="dropdown-item" href=" {{ route('user.signup') }} ">Sign Up</a> --}}
                     
                <div>
                    <li class="nav-item">
                        <a class="nav-link" href=" {{ route('user.signin') }} ">
                            Sign In
                        </a>
                    </li>
                </div>
            
                @endif  
                    
                
            </ul>

        </div>
    </nav>
</header>
