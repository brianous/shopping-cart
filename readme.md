# Shopping-Cart

[![Laravel](https://img.shields.io/badge/Laravel-%5E8.0-brightgreen)](https://laravel.com/)
<img src="https://img.shields.io/badge/php-%5E7.2-brightgreen">
<img src="https://img.shields.io/badge/Boostrap-4.5-brightgreen" >

## Table of Contents

- [Description](#description)
- [Screenshot](#screenshot)
    - [Sign up Page](#sign_up_page)
    - [Product Page](#sign_up_page)
    - [Shopping-cart Page](#shopping-cart_page)
    - [Password Assistance Page](#password_assistance_page)
    
- [Project setup](#project_setup)

## Description

A Shopping-Cart DEMO .

This is a Laravel web application.

<br>

## Screenshot 
### Sign up Page
![image](/readme_image/sign_up.gif)
    
    function => sign up / sign in / log out

### Product Page
![image](/readme_image/message_board.gif)
    
    function => message board

### Shopping-cart Page
![image](/readme_image/shopping-cart.gif)
    
    function => shopping cart

### Password Assistance Page
![image](/readme_image/password_assistance.png)
![image](/readme_image/password_assistance_letter.png)


## Project setup

1. revise the .env

2. 
    ```
    php artisan serve
    ```