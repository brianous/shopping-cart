<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User([
            'email'=> 'test',
            'password'=> Hash::make(1),
            'name'=>'test',
            'birthday'=>'1999/12/1',
        ]);
        $user->save();
    }
}
