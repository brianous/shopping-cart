<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = new \App\Product([
            'imgPath'=>'https://cdn.pixabay.com/photo/2015/05/07/15/08/pastries-756601_1280.jpg',
            'title'=>'cookie',
            'description'=>'It is a delicious cookie',
            'price'=>'1'
        ]);
        $product->save();
        
        $product = new \App\Product([
            'imgPath'=>'https://cdn.pixabay.com/photo/2015/05/07/15/08/pastries-756601_1280.jpg',
            'title'=>'cookie',
            'description'=>'It is a delicious cookie',
            'price'=>'2'
        ]);
        $product->save();

        $product = new \App\Product([
            'imgPath'=>'https://cdn.pixabay.com/photo/2015/05/07/15/08/pastries-756601_1280.jpg',
            'title'=>'cookie',
            'description'=>'It is a delicious cookie',
            'price'=>'3'
        ]);
        $product->save();
        $product = new \App\Product([
            'imgPath'=>'https://cdn.pixabay.com/photo/2015/05/07/15/08/pastries-756601_1280.jpg',
            'title'=>'cookie',
            'description'=>'It is a delicious cookie',
            'price'=>'4'
        ]);
        $product->save();

        $product = new \App\Product([
            'imgPath'=>'https://cdn.pixabay.com/photo/2015/05/07/15/08/pastries-756601_1280.jpg',
            'title'=>'cookie',
            'description'=>'It is a delicious cookie',
            'price'=>'5'
        ]);
        $product->save();

    }
}
